#!/bin/bash
function EC02(){
        OPERATIONS_EC=$1
        #cp /opt/equinoxAS/tmp/$OPERATIONS_EC.jar /opt/equinoxAS/lib/$OPERATIONS_EC.jar
        sed -i "s/directory=\"\/opt\/equinoxAS\/lib\/\" name = \"plus.jar\"/directory=\"\/opt\/equinoxAS\/lib\/\" name = \"$OPERATIONS_EC.jar\"/" /opt/equinoxAS/conf/trans01.EC02.0.0
        sed -i "s/JavaLibrary value=\"plus.jar\"/JavaLibrary value=\"$OPERATIONS_EC.jar\"/" /opt/equinoxAS/conf/trans01.EC02.0.0
        sed -i "s/<mode value = \"terminator\"/<mode value = \"$EC02_MODE\"/" /opt/equinoxAS/conf/trans01.EC02.0.0
}

if [ "$ES05_NEXT_PORT" == "" ]; then
    ES05_NEXT_PORT="8080"
fi

#EC02_MODE=[transporter,terminator]
if [ "$EC02_MODE" == "" ]; then
    EC02_MODE="terminator"
fi

#minus,multiply,divide,plus
case $OPERATION in
  plus) EC02 plus
   ;;
  minus) EC02 minus
   ;;
  multiply) EC02 multiply
   ;;
  divide) EC02 divide
   ;;
  *) EC02 plus
   ;;
esac

ES05_NPORT="host=\"ES05NEXTHOST\" port=\"$ES05_NEXT_PORT\""
OLD='host="ES05NEXTHOST" port="20200"'
sed -i "s/$OLD/$ES05_NPORT/" /opt/equinox/conf/trans01.E04.0.0

##################################################################

su - toro -c "eqx trans01 start"
while true
do
  sleep 30
done

##################################################################

