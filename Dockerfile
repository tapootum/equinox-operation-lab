#FROM equinoxrhel6
FROM tapootum/equinoxrhel6
COPY env /root/env
RUN mv /root/env/conf/* /opt/equinox/conf/ && \
    mv /root/env/jar/* /opt/equinoxAS/lib/ && \
    mv /root/env/conf/trans01.EC02.0.0 /opt/equinoxAS/conf/trans01.EC02.0.0 && \
    chown toro:toro -R /opt/equinox/conf/ /opt/equinoxAS/lib/ /opt/equinoxAS/conf && \
    rm -f /opt/equinox/conf/trans01.EC02.0.0 && \
    su - toro -c 'ln -s /opt/equinoxAS/conf/trans01.EC02.0.0 /opt/equinox/conf/trans01.EC02.0.0' && \
    export DOCKER="YES" && export DOCKER_OS="el6" && rpm -ivh /root/env/equinox-api-1.0.1-00.00.el6.x86_64.rpm
    #mv /root/env/eqxapi /etc/init.d/ && chmod +x /etc/init.d/eqxapi  && chkconfig eqxapi on && \
#COPY docker-entrypoint.sh /usr/local/bin/
 
ENTRYPOINT bash /opt/equinox/api/start && bash /root/env/docker-entrypoint.sh && bash
#CMD ["/root/env/docker-entrypoint.sh"]


