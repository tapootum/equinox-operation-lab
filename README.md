# Docker build
### docker build -t equinoxoperation .

## Example docker compose

```yml
version: '3.1'
services:
  eqxdivide1:
    image: equinoxoperator
    ports:
      - "9997:8080"
    environment:
      - EC02_MODE=transporter
      - OPERATION=divide
    links:
    - eqxmultiply1:ES05NEXTHOST

  eqxmultiply1:
    image: equinoxoperator
    environment:
      - EC02_MODE=transporter
      - OPERATION=multiply
    links:
    - eqxmultiply2:ES05NEXTHOST

  eqxmultiply2:
    image: equinoxoperator
    environment:
      - EC02_MODE=terminator
      - OPERATION=multiply
```